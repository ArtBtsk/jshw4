function calculate(num1, num2, operator) {
  let result;

  switch (operator) {
    case "+":
      result = num1 + num2;
      break;
    case "-":
      result = num1 - num2;
      break;
    case "*":
      result = num1 * num2;
      break;
    case "/":
      if (num2 !== 0) {
        result = num1 / num2;
      } else {
        console.log("Недопустима операція: ділення на нуль");
        return;
      }
      break;
    default:
      console.log("Невірна математична операція");
      return;
  }

  return result;
}

let number1 = parseFloat(prompt("Введіть перше число:"));
let number2 = parseFloat(prompt("Введіть друге число:"));

let operator = prompt("Введіть математичну операцію (+, -, *, /):");

let result = calculate(number1, number2, operator);

console.log("Результат:", result);
